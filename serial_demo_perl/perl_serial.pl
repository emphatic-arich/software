#!/usr/bin/perl
#
# perl example for interacting with picoblaze monitor
# assumptions:
# each command is a single line sent to the board terminated with \r
# each command results in a number of lines:
#   1.  echo of command
#   2.  zero or more lines containing data
#   3.  ">"
#
# subroutines defined:
# flush( $port)                  clear any pending input
# @r do_cmd( $port, $cmd)        send $cmd, return list of response strings
#                                returns empty list if no data
#

use Device::SerialPort;
use strict;

my $port = Device::SerialPort->new( "/dev/ttyACM0");

$port->baudrate(9600);

# these are probably defaults
$port->databits(8);
$port->parity("none");
$port->stopbits(1);

sub flush {
    my $p = shift @_;
    my $count;
    my $saw;
    while(1) {
	($count,$saw) = $p->read(255);
	last if $count == 0;
    }
}

sub clean_resp {
    my $r = shift @_;
    my @resp;
    foreach my $str ( split /\n/, $r ) {
	$str =~ s/[[:cntrl:]]//g;
	push @resp, $str;
    }
    return( () ) if( $#resp == 1);
    return @resp[1..$#resp-1];
}


sub do_cmd {
    my ($p, $cmd) = @_;
    my $buffer = "";
    $p->write("$cmd\r");
    # read to ">" prompt
    while(1) {
	my ($count,$saw) = $p->read(255);
	if( $count > 0) {
	    $buffer .= $saw;
	    if( $buffer =~ />/) {
		return( clean_resp($buffer));
	    }
	}
    }
}

flush( $port);

#
# example to read commands from terminal and execute them on the board
#
while(1) {
    my $cmd = <STDIN>;
    chomp( $cmd);
    my @r = do_cmd( $port, $cmd);
    if( $#r == -1) {
	print "No response\n";
    } else {
	foreach my $s ( @r ) {
	    print "resp: $s\n";
	}
    }
}
