#!/usr/bin/perl
#
# read a CSV file with bit assignments for CITIROC test firmware
# look for groups of 4 fields starting with a connector designation
# in the format "Jnn-nn".  Fields are:
#
#   Connector pin on Trenz breakout board
#   Net name
#   Address
#   Bit number
#
# Addresses 0, 1 are write for outputs
# Addresses 2-5 are read-only for inputs
#

use Text::CSV qw( csv );
use Data::Dumper;

# read entire CSV file and return an array of arrays
my $aoa = csv ( in => $ARGV[0]);

for $row ( @{$aoa}) {		# loop over rows
    my $ncol = $#{$row};
    next if( $ncol < 4);	# skip short rows
    for( my $i=0; $i<$ncol; $i++) { # loop over columns
	if( $row->[$i] =~ /^J\d/) { # check for a connector name
	    # found a group
	    my $conn = $row->[$i]; # connector name
	    my $net = $row->[$i+1]; # net/pin name
	    my $adx = $row->[$i+2]; # address
	    my $bit = $row->[$i+3]; # bit
	    # validate lengths
	    if( length($adx) && length($bit)) {
		# we now have a valid table entry
		# we could output this in any convenient format...
		print "Conn: $conn  $net: $net  Adr: $adx  Bit: $bit\n";
	    }
	}
    }
}
