
typedef struct {
  char *name;
  int start_bit;
  int len;
  unsigned value;
} a_register;

void croc_chomp( char *str);
int croc_parse( char* str, a_register* res);
int croc_dump( a_register* r);
unsigned long convert_value( char* str);
