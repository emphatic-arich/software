

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <errno.h>

#include "croc_fun.h"

int main( int argc, char *argv[]) {

  FILE *fp;
  char buff[256];
  int rc;

  a_register *list_of_regs;
  a_register reg;

  int num_regs = 0;
  int i;

  if( (fp = fopen( argv[1], "r")) == NULL) {
    printf("Can't open %s for input\n", argv[1]);
    exit(1);
  }

  /* first pass... count number of register defs with bit start/length */

  while( fgets( buff, sizeof( buff)-1, fp)) {
    croc_chomp( buff);
    if( croc_parse( buff, &reg) == 2)
      ++num_regs;
  }
  printf("Pass 1:  found %d register definitions\n", num_regs);

  list_of_regs = calloc( num_regs, sizeof(a_register));
  i = 0;

  rewind( fp);

  while( fgets( buff, sizeof( buff)-1, fp)) {
    croc_chomp( buff);
    if( croc_parse( buff, &reg) == 2) {
      memcpy( &list_of_regs[i], &reg, sizeof(a_register));
      printf( "Copy from: ");  croc_dump( &reg);
      printf( "Copy to:   ");  croc_dump( &list_of_regs[i]);
      ++i;
    }
  }

  for( i=0; i<num_regs; i++)
    croc_dump( &list_of_regs[i]);

}
