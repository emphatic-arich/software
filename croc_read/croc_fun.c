#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <ctype.h>
#include <stdlib.h>
#include <errno.h>

#include "croc_fun.h"

/*
 * parse a register name and value(s)
 * valid input formats:
 *   <name>  <value>    
 *   <name>  <start_bit> <length> <default_value>
 * anything after "#" is a comment, ignored
 *
 * return:
 *  
 *  -1 if parse error
 *   0 if comment or blank line
 *   1 if just a new value
 *   2 if includes start bit and length
 */

int croc_parse( char* str, a_register* res)
{
  char *cp, *nam, *p1, *p2, *p3;

  /* skip empty lines or lines starting with comment */
  if( str[0] == '#' || str[0] == '\0' || iscntrl( str[0]))
      return 0;

  /* parse into tokens */
  nam = strtok( str, " \t");
  if( !isalpha( *nam)) {
    printf("Expected register name, saw %s\n", nam);
    return -1;
  }

  res->name = strdup( nam);

  /* parse tokens after name */
  p1 = strtok( NULL, " \t");
  p2 = strtok( NULL, " \t");
  p3 = strtok( NULL, " \t");

  /* check for at least one value */
  if( (p1 == NULL) || !isdigit( *p1) || (*p1 == '#') ) {
    printf("Expected at least a value after %s, saw: \"%s\"\n", nam, p1);
    return -1;
  }

  if( (p2 == NULL) || (*p2 == '#')) {
    /* only one value parameter */
    res->value = convert_value( p1);
    return 1;
  }

  /* now there should be 3 parameters, all numeric */
  if( !isdigit( *p1) || !isdigit( *p2) || !isdigit( *p3)) {
    printf( "Expected 3 numbers after %s, got (%s,%s,%s)\n", nam, p1, p2, p3);
    return 0;
  }

  /* convert and return */
  res->start_bit = convert_value( p1);
  res->len = convert_value( p2);
  res->value = convert_value( p3);

  return 2;
}

/*
 * dump register value for debug
 */
int croc_dump( a_register* r)
{
  printf("%-40s start=%4d len=%3d value=%6d (0x%08x)\n",
	 r->name, r->start_bit, r->len, r->value, r->value);
}




/*
 * convert an unsigned decimal or hex value to unsigned long
 * check for invalid characters first, then use strtoul() to convert
 * die with error message on conversion error
 */
unsigned long convert_value( char* str) {
  char *p;
  unsigned long v;

  //  printf("Convert \"%s\"\n", str);

  if( !*p)
    return 0;

  /* better either start with a digit and consist only of digits, or "0x" */
  if( !isdigit( *str)) {
    printf("Value %s doesn't start with a digit\n", str);
    exit( 1);
  }
  if( str[0] == '0' && toupper(str[1]) == 'X') {
    /* it's hex, check for hex digits */
    for( p=str+2; *p; p++) {
      if( !isxdigit( *p)) {
	printf("Expecting hex digit in %s, got '%c'\n", str, *p);
	exit(1);
      }
    }
  } else {
    /* it's decimal, check for decimal digits */
    for( p=str; *p; p++) {
      if( !isdigit( *p)) {
	printf("Expecting decimal digit in %s, got '%c'\n", str, *p);
	exit(1);
      }
    }
  }

  /* check for errors in strtoul() by clearing errno as described in man page */
  errno = 0;
  v = strtoul( str, NULL, 0);
  if( errno) {
    printf("Error converting \"%s\" to a number\n", str);
    printf("%s\n", strerror( errno));
    exit( 1);
  }
  //  printf("Converted to %ld\n", v);
  return v;
}


/*
 * implement the something like "chomp" perl function:  
 * remove trailing control characters 
*/
void croc_chomp( char *str)
{
  char *p;
  if( strlen( str))
    for( p=str+strlen(str)-1; *p && iscntrl(*p); --p)
      *p = '\0';
}
