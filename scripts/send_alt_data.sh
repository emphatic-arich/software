#!/bin/bash
#
# simple script to test out CITIROC serial interface
#

# set this to the active port
PORT=$1

if [ "$#" -ne 1 ]; then
    echo "Need a port, e.g. /dev/ttyUSB0"
    exit
fi

if test -c ${PORT}; then
    echo "${PORT} exists"
else
    echo "${PORT} doesn't exist"
    exit
fi    

# function to echo a string, then send to port with <CR> appended
send() {
    echo $1 $'\015'
    echo $1 $'\015' > ${PORT}
}

while true
do
    send "a 70"			# set start address
    send "d 1"			# binary 1101 to start
    send "a fc"			# set end address
    send "d 800000"			# binary 1011 to end

    send "c 0"
    sleep 0.5

    send "a 70"			# set start address
    send "d 2"			# binary 1101 to start
    send "a fc"			# set end address
    send "d 400000"			# binary 1011 to end

    send "c 0"
    sleep 0.5

    send "a 70"			# set start address
    send "d 4"			# binary 1101 to start
    send "a fc"			# set end address
    send "d 200000"			# binary 1011 to end

    send "c 0"
    sleep 0.5
    
done
    
    

      
      
