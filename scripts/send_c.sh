#!/bin/bash
#
# simple script to test out CITIROC serial interface
#

# set this to the active port
PORT=$1

# check if there's a port specified
if [ "$#" -ne 1 ]; then
    echo "Need a port, e.g. /dev/ttyUSB0"
    exit
fi

if test -c ${PORT}; then
    echo "${PORT} exists"
else
    echo "${PORT} doesn't exist"
    exit
fi    

# function to echo a string, then send to port with <CR> appended
send() {
    echo $1 $'\015'
    echo $1 $'\015' > ${PORT}
}

send "a 70"			# set start address
send "d d"			# binary 1101 to start
send "a fc"			# set end address
send "d b00000"			# binary 1011 to end

# trigger serial output in a loop
while true
do
    send "c 0"
    sleep 0.5
done


      
      
