//
// very simple class to support reading CITIROC registers
//
#include <iostream>
#include <sstream>
#include <vector>
#include <string>

#include <stdio.h>

#include "croc.hh"

//
// helper function to tokenize a string with whitespace delimiters
//
std::vector <std::string> tokenize( std::string str) {

  std::vector<std::string> tokens;
  std::stringstream s1( str);
  std::string tmp;

  while( getline( s1, tmp, ' ')) {
    if( tmp.length())		// don't save empty tokens
      tokens.push_back( tmp);
  }

  return tokens;
}
