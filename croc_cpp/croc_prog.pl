#!/usr/bin/perl
#
# read CROC registers from CSV and convert to bitstring
# create a download script
#
# Expects the following columns in the CSV:
#   "Register Name"        any quoted string
#   "First Bit Number"     decimal 0-1143
#   "Register Width"       decimal 1-32
#   "Default Value"        hex (0x), binary (0b), decimal
#   "Reverse"              "Yes" or "No" to reverse bit order
#

use strict;
use Text::CSV qw( csv );
use Data::Dumper;

my $reverse = 1;		# set to 1 to reverse the entire bitstring
my $debug = 0;			# debug output


# length of bitstring
my $nbits = 1144;
# create an array of zeroes
my @bits = (0)x$nbits;

print $#bits+1, " bits\n" if($debug);


#---------- subroutines ----------

# convert binary to decimal (up to 32 bits)
sub bin2dec {
    return unpack("N", pack("B32", substr("0" x 32 . shift, -32)));
}

# convert 0x, 0b or decimal to integer
sub any2dec {
    my $val = shift;
    my $ival = -1;
    $ival = hex( substr( $val, 2)) if( $val =~ /^0x/i);      # hex?
    $ival = bin2dec( substr( $val, 2)) if ( $val =~ /0b/i);  # binary?
    $ival = $val if( $val =~ /^\d+$/);                       # decimal
    return $ival;
}

#
# stuff_bits( @bits, $frst, $wid, $val, $rev);
#   treat @bits as a bit array
#   overwrite $wid bits starting at $frst
#   with bits from integer $val
#   if($rev) count from end of @bits
#
sub stuff_bits {
    my ( $s, $f, $w, $v, $r) = @_;
    my $nb = @{$s};

    print "stuff_bits( $nb, $f, $w, $v, $r)\n" if($debug);

    my $b = 1;
    for( my $i=0; $i<$w; $i++) {
	my $idx = $i+$f;
	$idx = ($nb-1) - $idx if( $r);
	my $val = ($v & $b) ? 1 : 0;
	$s->[$idx] = $val;
	print "SET idx=$idx -> $val\n" if( $debug);
	$b <<= 1;
    }
}


#-------------------------------------


#---------- print bash script header ----------

print q{#!/bin/bash
# set this to the active port
PORT=$1

# check if there's a port specified
if [ "$#" -ne 1 ]; then
    echo "Need a port, e.g. /dev/ttyUSB0"
    exit
fi

if test -c ${PORT}; then
    echo "${PORT} exists"
else
    echo "${PORT} doesn't exist"
    exit
fi    

# function to echo a string, then send to port with <CR> appended
send() {
    echo $1 $'\015'
    echo $1 $'\015' > ${PORT}
}

send "a 70"			# set start address
};

#---------- end of bash script header ----------

#---------- main ------------------------------

# read the entire CSV file into an array of hashes
my $aoh = csv( in=> $ARGV[0], headers => "auto");

my $nrow = @{$aoh};
print "# $nrow rows read\n";
my $row0 = $aoh->[0];		# get first row

# check for required columns
die "Missing required column heading"
    if( !exists $row0->{'Register Name'} || !exists $row0->{'First Bit Number'} ||
 	!exists $row0->{'Register Width'} || !exists $row0->{'Default Value'} ||
 	!exists $row0->{'Reverse'});

#
# parse register values
#
foreach my $reg ( @{$aoh}) {

    my $nam = $reg->{'Register Name'};
    my $frst = $reg->{'First Bit Number'};
    my $wid = $reg->{'Register Width'};
    my $val = $reg->{'Default Value'};
    my $rev = $reg->{'Reverse'};
    my $ival = any2dec( $val);

    # stuff into @bits
    # set $b to one end of item bit range based on reverse

    stuff_bits( \@bits, $frst, $wid, $val, $reverse);
}


#
# debug:  print the string with details
#
if( $debug) {
    for( my $i=0; $i<$nbits; $i++) {
	my $regnam = "??";
	my $offs = -1;
	foreach my $reg ( @{$aoh}) {
	    my $nam = $reg->{'Register Name'};
	    my $frst = $reg->{'First Bit Number'};
	    my $wid = $reg->{'Register Width'};
	    if( $i >= $frst && $i < $frst+$wid) {
		$regnam = $nam;
		$offs = $i - $frst;
	    }
	}
	printf "# %4d: %d (%s bit %d)\n", $i, $bits[$i], $regnam, $offs;
    }
}


#
# output a loader script
#
for( my $i=0; $i<$nbits; $i+=32) {
    my $b = 1;
    my $v = 0;
    for( my $n=0; $n<32; $n++) {
	if( $bits[$i+$n]) {
	    $v |= $b;
	}
	$b <<= 1;
    }
    printf "send \"d %08x\"   # %d\n", $v, $i;
}
