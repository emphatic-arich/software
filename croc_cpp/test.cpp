// Tokenizing a string using stringstream
#include <iostream>

#include "croc.hh"
  
using namespace std;
  
int main()
{
  string line;

  while( getline( cin, line)) {
    cout << "Line: \"" << line << "\"" << endl;

    // Vector of string to save tokens
    std::vector <std::string> tokens;

    tokens = tokenize( line);

    // Printing the token vector
    cout << "----------" << tokens.size() << "--------" << endl;
    for(int i = 0; i < tokens.size(); i++)
      cout << tokens[i] << '\n';
  }

      
}
