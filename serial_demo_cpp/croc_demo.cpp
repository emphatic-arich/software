//
// croc_demo.cpp
//
#include <unistd.h>

#include "serial_command.hh"

using namespace LibSerial;
using namespace std;

int main( int arg, char *argv) {

  string cmd;			// command to send
  string resp;			// response received
  
  SerialPort sp( "/dev/ttyUSB3"); // connect to the port
  sp.SetBaudRate( LibSerial::BaudRate::BAUD_9600); // set baud rate

  vector<string> rv;

  // initialize
  rv = command_vector( sp, "a 70");
  rv = command_vector( sp, "d d");
  rv = command_vector( sp, "a fc");
  rv = command_vector( sp, "d b00000");
  
  while( 1) {

    cout << "Enter command: ";	// get command from user
    getline( cin, cmd);

    while(1) {

      vector<string> rv = command_vector( sp, cmd);

      if( rv.size() == 0)
	cout << "(empty)" << endl;
      else {
	for( unsigned i=0; i<rv.size(); i++)
	  cout << i << " = " << rv[i] << endl;
      }

      sleep(1);

    }
  }
  
}
