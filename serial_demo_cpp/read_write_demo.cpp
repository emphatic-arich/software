//
// read_write_demo.cpp
//

#include "serial_command.hh"
#include "serial_read_write.hh"
#include "Bitbang_Firmware_Lookup.hh"
#include <sstream>
#include <unistd.h> // for sleep()

using namespace LibSerial;
using namespace std;

// split a string into a vector of tokens using a separator
vector<string> split_string(string s, char sep)
{
  string tmp;
  stringstream ss(s);
  vector<string> rv;
  while (getline(ss, tmp, sep))
    rv.push_back(tmp);
  return (rv);
}

int main()
{
  uint32_t addr, data;
  string cmd, line;
  vector<string> v;

  SerialPort sp("/dev/ttyUSB0");                  // connect to the port
  sp.SetBaudRate(LibSerial::BaudRate::BAUD_9600); // set baud rate

  vector<BitInfo> table;
  table = Table("Bitbang_Firmware_Pinout_Bits.csv"); // set lookup table

  cout << "Enter 'R <addr>' or  'W <addr> <data>' or '<NetName> <value>'" << endl;
  cout << "  decimal or 0xHEX" << endl;

  while (1)
  {

    cout << "> ";
    getline(cin, cmd);
    v = split_string(cmd, ' ');
    int i;

    i = find_name(v[0], table); // check if first entry is netname

    //if entry is not name in table, continue with traditional format check
    if (i < 0)
    {
      if (v.size() > 1)
      {
        addr = strtoul(v[1].c_str(), NULL, 0);
        if (v.size() > 2)
        {
          data = strtoul(v[2].c_str(), NULL, 0);
          printf("Got data 0x%x\n", data);
        }
      }

      if (v[0] == "r" || v[0] == "R")
      {
        data = serial_register_read(sp, addr);
        printf("Read from 0x%x\n", addr);
        printf("Read data = %d (0x%08x)\n", data, data);
      }
      else if (v[0] == "w" || v[0] == "W")
      {
        printf("Write 0x%x to 0x%x\n", data, addr);
        serial_register_write(sp, addr, data);
      }
      else
      {
        cout << "Unknown command: " << v[0] << endl;
      }
    }

    //If first word was a NetName in the table
    else
    {
      if (v.size() == 2)
      {
        //set address and data from table
        addr = table[i].Adx;
        data = table[i].Bit;

        //read current status of address
        //uint32_t stat =  serial_register_read( sp, addr);

        if (v[1] == "0")
        {
          serial_register_write(sp, addr, 0);
        }
        else if (v[1] == "1")
        {
          serial_register_write(sp, addr, 1 << data);
        }
        else if (v[1] == "T" || v[1] == "t")
        {
          while (1)
          {
            //getline(cin, quit);
            serial_register_write(sp, addr, 1 << data);
            sleep(1);
            serial_register_write(sp, addr, 0);
            sleep(1);
          }
        }
        else
        {
          cout << "Unknown command. <value> should be 0 or 1";
        }
        printf("Address is: %x\n", addr);
        printf("1<<data = %d\n", 1 << data);
      }
      else
      {
        cout << "Unknown command. Enter '<NetName> <value>'";
      }
    }
  }
}
