// serial_blink.cpp -- blink the LEDs on the board

#include <libserial/SerialPort.h> // use library from "libserial-dev" pkg
#include <unistd.h>		  // for sleep()

using namespace LibSerial;
using namespace std;

// easiest to declare a short function to send a string to the port
void send_string_cmd( SerialPort& sp, std::string str) {
    vector<uint8_t> vec( str.begin(), str.end()); // need a vector to write
    sp.Write( vec);
}  

int main() {
  SerialPort sp( "/dev/ttyUSB0"); // connect to the port
  sp.SetBaudRate( LibSerial::BaudRate::BAUD_9600); // set baud rate

  while( 1) {
    send_string_cmd( sp, "W 0 40000000\r"); // command to turn on LEDs
    sleep(1);
    send_string_cmd( sp, "W 0 0\r"); // command to turn off LEDs
    sleep(1);
  }
}



