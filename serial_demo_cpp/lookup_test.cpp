#include "Bitbang_Firmware_Lookup.hh"

using namespace std;

int main(){
  string name;
  vector<BitInfo> x;
  int i;
  
  x = Table( "Bitbang_Firmware_Pinout_Bits.csv");

  cout << "Enter name to translate or 'list'" << endl;

  while(1) {
    cout << "Name: ";
    getline( cin, name);
    if( name == "list" || name == "LIST") {
      for(int i=0; i < x.size(); i++)
	std::cout << x[i].NetName << ' '<< x[i].Adx<<' '<< x[i].Bit<<std::endl;
    } else {
      i = find_name( name, x);
      if( i < 0)
	cout << "Not found!" << endl;
      else
	cout << "Addr: " << x[i].Adx << " Bit: " << x[i].Bit << endl;
    }
  }

} 
