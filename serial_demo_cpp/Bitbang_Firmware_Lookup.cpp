#include "Bitbang_Firmware_Lookup.hh"

using namespace std;


vector<BitInfo> Table( string fname){
  // Open CSV file
  std::ifstream file( fname);
  if( file.fail()) {
    cout << "Couldn't open file " << fname << endl;
    exit(1);
  }

  // Define Column variables
  string ConPin1, ConPin2, ConPin3;
  string NetName1, NetName2, NetName3;
  string Adx1, Adx2, Adx3;
  string Bit1, Bit2, Bit3;
  string empty1, empty2;

  // Define return vector using struct
  vector<BitInfo> BitLookup;

  while(file.good()){
    //Reading each cell based on format of CSv
    getline(file,ConPin1,',');
    getline(file,NetName1,',');
    getline(file,Adx1,',');
    getline(file,Bit1,',');
    getline(file,empty1,',');
    getline(file,ConPin2,',');
    getline(file,NetName2,',');
    getline(file,Adx2,',');
    getline(file,Bit2,',');
    getline(file,empty2,',');
    getline(file,ConPin3,',');
    getline(file,NetName3,',');
    getline(file,Adx3,',');
    getline(file,Bit3,'\n');

    //Removing quotation marks from strings
    ConPin1.erase(remove( ConPin1.begin(), ConPin1.end(), '\"' ),ConPin1.end());
    NetName1.erase(remove( NetName1.begin(), NetName1.end(), '\"' ),NetName1.end());
    ConPin2.erase(remove( ConPin2.begin(), ConPin2.end(), '\"' ),ConPin2.end());
    NetName2.erase(remove( NetName2.begin(), NetName2.end(), '\"' ),NetName2.end());
    ConPin3.erase(remove( ConPin3.begin(), ConPin3.end(), '\"' ),ConPin3.end());
    NetName3.erase(remove( NetName3.begin(), NetName3.end(), '\"' ),NetName3.end());

    //Only push to vector if ConPin has a name and the address is not blank
    if (ConPin1 != "ConPin" && ConPin1 != "" && Adx1 != ""){
      transform( NetName1.begin(), NetName1.end(), NetName1.begin(), ::toupper);
      BitLookup.push_back({NetName1,std::stoi( Adx1 ),std::stoi( Bit1 )}); //std::stio() converts string to int
    }
    if (ConPin2 != "ConPin" && ConPin2 != "" && Adx2 != ""){
      transform( NetName2.begin(), NetName2.end(), NetName2.begin(), ::toupper);
      BitLookup.push_back({NetName2,std::stoi( Adx2 ),std::stoi( Bit2 )}); //std::stio() converts string to int
    }
    if (ConPin3 != "ConPin" && ConPin3 != "" && Adx3 != ""){
      transform( NetName3.begin(), NetName3.end(), NetName3.begin(), ::toupper);
      BitLookup.push_back({NetName3,std::stoi( Adx3 ),std::stoi( Bit3 )}); //std::stio() converts string to int
    }

    // clear all strings
    ConPin1.clear();
    NetName1.clear();
    Adx1.clear();
    Bit1.clear();
    ConPin2.clear();
    NetName2.clear();
    Adx2.clear();
    Bit2.clear();
    ConPin3.clear();
    NetName3.clear();
    Adx3.clear();
    Bit3.clear();
  }

  return BitLookup;
}

// lookup name, return index or -1 if not found
int find_name( string name, vector<BitInfo> v) {
  std::transform( name.begin(), name.end(), name.begin(), ::toupper);
  for(int i=0; i < v.size(); i++) {
    if( name == v[i].NetName)
      return i;
  }
  return -1;
}
