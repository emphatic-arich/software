
#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>

using namespace std;

struct BitInfo {
  std::string NetName;
  int Adx;
  int Bit;
};

vector<BitInfo> Table( string fname);
int find_name( string name, vector<BitInfo> v);
