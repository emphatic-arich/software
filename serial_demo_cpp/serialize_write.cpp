// This program takes user inputs for the values for 'data', 'clk_div', 'bit_count', 'direction'
// and sends them to the Trenz module through USB.
#include "serial_read_write.hh"
#include <vector>
#include <unistd.h>
#include <iostream>

int main()
{
    SerialPort sp("/dev/ttyUSB0");                  // connect to the port
    sp.SetBaudRate(LibSerial::BaudRate::BAUD_9600); // set baud rate
    // Four commands to be sent to Trenz:
    string data;
    string clk_div;
    string bit_count;
    string direction;
    while (1)
    {
        cout << "\nEnter the value for: data\n";
        getline(cin, data);
        cout << "\nEnter the value for: clk_div\n";
        getline(cin, clk_div);
        cout << "\nEnter the value for: bit_count\n";
        getline(cin, bit_count);
        cout << "\nEnter the value for: direction\n";
        getline(cin, direction);
        cout << "\n Command Sent\n";

        serial_register_write(sp, 0, 0x10000000); // To select mode
        serial_register_write(sp, 0x30000000, strtoul(data.c_str(), NULL, 0));
        serial_register_write(sp, 0x30000001, strtoul(clk_div.c_str(), NULL, 0));
        serial_register_write(sp, 0x30000002, strtoul(bit_count.c_str(), NULL, 0));
        serial_register_write(sp, 0x30000010, 1); // 'Start' signal
    }
    return 0;
}