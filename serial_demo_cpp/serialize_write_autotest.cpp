// This program takes user inputs for the values for 'data', 'clk_div', 'bit_count', 'direction'
// and sends them to the Trenz module through USB.
#include "serial_read_write.hh"
#include "serial_command.hh"
#include <vector>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>

int main( int argc, char *argv[])
{
  SerialPort sp("/dev/ttyUSB0");                  // connect to the port
  sp.SetBaudRate(LibSerial::BaudRate::BAUD_9600); // set baud rate
  // Four commands to be sent to Trenz:
  string data;
  string clk_div;
  string bit_count;
  string direction;

  if( argc < 2) {
    cout << "usage:  ser..test -r  (issue reset reset)" << endl;
    cout << "                  -n  (skip reset)" << endl;
    exit(0);
  }

  switch( toupper( argv[1][1])) {
  case 'N':
    cout << "skipping reset" << endl;
    break;
  case 'R':
    cout << "sending reset \"O 10 0\"" << endl;
    send_string_cmd( sp, "O 10 0");
    break;
  default:
    cout << "Unknown option" << endl;
    exit(0);
  }

  cout << "set bit 28 for SIO mode" << endl;
  serial_register_write(sp, 0, 0x10000000); // To select mode

  while (1)
    {
      serial_register_write(sp, 0x30000000, 0x55555555);
      usleep(100000);
      serial_register_write(sp, 0x30000001, 0x0000000a);
      usleep(100000);
      serial_register_write(sp, 0x30000002, 0x00000008);
      usleep(100000);
      serial_register_write(sp, 0x30000010, 0); // 'Start' signal
      cout << "Trigger Serial start" << endl;
      usleep(100000);
    }
  return 0;
}
