//
// serial_read.cpp
//

#include "serial_command.hh"
#include <unistd.h> // for sleep()

int main()
{
    string cmd;
    cout << "Which address do you want to read? Enter 'R <Address>' \n >";
    getline(cin, cmd);

    SerialPort sp("/dev/ttyUSB0");                  // connect to the port
    sp.SetBaudRate(LibSerial::BaudRate::BAUD_9600); // set baud rate
    while (1)
    {
        cout << command_response(sp, cmd);
        sleep(0.25);
    }
}