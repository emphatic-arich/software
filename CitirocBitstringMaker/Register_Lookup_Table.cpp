#include "Register_Lookup_Table.hh"
#include <cstring>

using namespace std;


vector<RegInfo> Table( string fname){
  // Open CSV file
  std::ifstream file( fname);
  if( file.fail()) {
    cout << "Couldn't open file " << fname << endl;
    exit(1);
  }

  // Define Column variables
  string RegName;
  string BitNum;
  string RegWidth;
  string RegValue;

  // Define return vector using struct
  vector<RegInfo> RegLookup;
  int line_number=-1;

  while(file.good()){
    line_number+=1;
    //Reading each cell based on format of csv
    getline(file,RegName,',');
    getline(file,BitNum,',');
    getline(file,RegWidth,',');
    getline(file,RegValue,'\n');

    //Remove last character from RegValue (this is a return character that is not inteded to be there)
    RegValue.pop_back();

    //skips first line with if statement and stops at end of file
    if(line_number!=0 && line_number<324){
      //Push RegName, BitNum, RegWidth, and ReValue to array
      transform( RegName.begin(), RegName.end(), RegName.begin(), ::toupper);
      RegLookup.push_back({RegName,std::stoi( BitNum ),std::stoi( RegWidth ),RegValue}); //std::stio() converts string to int
    }
    // clear all strings
    RegName.clear();
    BitNum.clear();
    RegWidth.clear();
    RegValue.clear();
  }

  return RegLookup;
}

// lookup name, return index or -1 if not found
int find_name( string name, vector<RegInfo> v) {
  std::transform( name.begin(), name.end(), name.begin(), ::toupper);
  for(int i=0; i < v.size(); i++) {
    if( name == v[i].Name)
      return i;
  }
  return -1;
}