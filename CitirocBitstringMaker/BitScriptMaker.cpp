#include "Register_Lookup_Table.hh"
#include "Number_Input_Check.hh"
#include <bitset>
#include <tgmath.h>
#include <tclap/CmdLine.h>
#include <iostream>
#include <string>
#include <fstream>
#include "../serial_demo_cpp/serial_command.hh"
#include "../serial_demo_cpp/serial_read_write.hh"

using namespace std;

int main(int argc, char** argv){
    /// First section of the code compiles the bitstring

    //Try block used to catch errors in command line entry
    try{

    TCLAP::CmdLine cmd("Call configuration file and run in test mode or with an active serial port", ' ', "0.9");

    // Configure input file to a command line argument
    TCLAP::ValueArg<std::string> inputfileArg("f","file","Configuration file to load",true,"homer","string");
    TCLAP::ValueArg<std::string> readinArg("r","inbitstring","Bitstring data file to load",false,"homer","string");    

    cmd.add( inputfileArg );
    cmd.add( readinArg );
    
    //Parse command line and extract inputs
    cmd.parse( argc, argv );
    std::string input_file = inputfileArg.getValue();
    std::string data_in = readinArg.getValue();

    //Initialize Variables for creating Bitstring
    string name;
    vector<RegInfo> Reg_Info;
    std::string total_bitstring;

    int error_flag=0;

    //Load default register values
    Reg_Info = Table(input_file);

    //Loop through all values in register
    for (int i=0; i< Reg_Info.size(); i++){
        // convert string to char* for input to NumberConvert
        char* RegValue_char= &Reg_Info[i].Value[0];

        //initialize number that will become integer for this register
        uint64_t num;

        if(NumberConvert(RegValue_char,num)){
            std::string bitstring = std::bitset<32>(num).to_string();

            //Return error if value is too large for register
            for(int j=0; j<bitstring.length()-Reg_Info[i].Width; j++){
                //if value has a 1 in a location that is out of register reach, return following error
                if(bitstring[j]=='1'){
                    cout<<"Error. Invalid input: "<< bitstring<<" for register with width "<<Reg_Info[i].Width<<". Line "<<i+1<<endl;
                    error_flag+=1;
                }
            }

            //Removing the first 32-(RegistedWidth) characters to have bitstring be the correct width
            bitstring.erase (0,(32-Reg_Info[i].Width));

	    //Correcting bitstring ordering
            std::reverse(bitstring.begin(), bitstring.end()); 
            
	    //Reversing Bitstring for HG_GAIN and LG_GAIN 
            if (Reg_Info[i].Name.find("G_GAIN") != std::string::npos) {
                std::reverse(bitstring.begin(), bitstring.end()); 
            }

            //leftmost entry is 0th bit
            total_bitstring.append(bitstring);
        }
        else{
            cout<<"Error listed above for value of register: "<<Reg_Info[i].Name<<" located on line "<<i+1<<"."<<endl;
        }

    }

    //If data file input is selected, overwrite default bitstring with bitstring from data file
    if (data_in!="homer"){
        ifstream data_infile(data_in);
        std::stringstream temp_bitstring;
        temp_bitstring << data_infile.rdbuf();
        total_bitstring = temp_bitstring.str();
        temp_bitstring.str("");
    }



    //check if bitstring is correct size
    if (total_bitstring.length()!=1144) {
        cout<< "Bitstring is not 1144 bits long."<<endl;
        error_flag+=1;
    }

    if(error_flag>0){ 
        cout<<"Unable to produce 1144 bitstring due to above errors"<<endl;
    }

    //Initialize string array to hold 32 bit hex values
    std::string hex_values[36]={};
    int loop_tracker =0;
    std::string sub_string;
    std::stringstream hex_string;
    std::string rev_hex_string;
    std::string bit_value;
    int j = -1;
    std::hex;

    //Loop backwards through array to create Hex values to send
    for (int i = total_bitstring.length(); i --> 0; ){
        loop_tracker+=1;
        bit_value = total_bitstring[i];
        sub_string.append(bit_value);
        //Need to set hex string every 16 bits to prevent errors with integers being too large
        if (loop_tracker==16){
	    std::reverse(sub_string.begin(), sub_string.end());
	    hex_string<<std::hex<<std::stoi(sub_string,0,2);
            sub_string="";
        }
        //Store string and reset every 32 bits
        if (loop_tracker==32 or i==0){
            j+=1;
	    //store previous stringstream in order to push new one to the front
            const std::string &temp = hex_string.str();
	    hex_string.seekp(0);
	    std::reverse(sub_string.begin(), sub_string.end());
	    hex_string<< std::stoi(sub_string,0,2);
	    hex_string<<temp;
            //rev_hex_string = hex_string.str();
            //std::reverse(rev_hex_string.begin(), rev_hex_string.end());
            //hex_values[j] = rev_hex_string;
            hex_values[j] = hex_string.str();
	    loop_tracker=0;
            sub_string="";
            hex_string.str("");
            rev_hex_string ="";
        }
    }
    cout<< "Writing to file: "<<"./SendString.sh"<<endl;

    //Write script for sending bitstring to Citiroc

    //open file for writing
    ofstream fw("./SendString.sh", std::ofstream::out);

    //check if file was successfully opened for writing
    if (fw.is_open()){
        fw << "#!/bin/bash"<< "\n";
        fw << "#"<< "\n";
        fw << "#Bash Script to send serial data to CitiRoc"<< "\n";
        fw << "#"<< "\n";
        fw << "\n";
        fw << "# set this to active port"<< "\n";
        fw << "PORT=$1"<< "\n";
        fw << "\n";
        fw << "# check if there's a port specified"<< "\n";
        fw << "if [ \"$#\" -ne 1 ]; then" << "\n";
        fw <<"\t"<< "echo \"Need a port, e.g. /dev/ttyUSB0\"" << "\n";
        fw <<"\t"<< "exit" << "\n";
        fw << "fi"<<"\n";
        fw << "\n";
        fw << "if test -c ${PORT}; then" << "\n";
        fw <<"\t"<< "echo \"${PORT} exists\"" << "\n";
        fw << "else"<<"\n";
        fw <<"\t"<< "echo \"${PORT} doesn't exist\"" << "\n";
        fw <<"\t"<< "exit" << "\n";
        fw << "fi"<<"\n";
        fw << "\n";
        fw << "# function to echo a string, then send to port with <CR> appended" << "\n";
        fw << "\n";
        fw << "send() {"<< "\n";
        fw <<"\t"<< "echo $1" << "\n";
        fw <<"\t"<< "echo $1  > ${PORT}" << "\n";
        fw << "}"<< "\n";
        fw << "\n";
	fw << "send \"x 1\""<<"\n";
        fw << "send \"a 70\""<< "\t"<< "\t"<<"# set start address"<< "\n";
        for (int i = 0; i < 36; i++){
             fw << "send \"d 0x"<< hex_values[i]<<"\""<<"\n";
        }
        fw << "\n";
        //fw << "while true"<< "\n";
        //fw << "do"<< "\n";
        fw <<"\t"<< "send \"c 0\""<<"\n";
        //fw <<"\t"<< "sleep 0.5"<<"\n";
        //fw << "\n";
        //fw <<"done"<<"\n";

        fw.close();
    }
    else cout << "Error opening file";

    }
    catch (TCLAP::ArgException &e) //catch exceptions
    {std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;}
    return 0;
}
