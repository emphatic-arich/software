#!/bin/bash
#
#Bash Script to send serial data to CitiRoc
#

# set this to active port
PORT=$1

# check if there's a port specified
if [ "$#" -ne 1 ]; then
	echo "Need a port, e.g. /dev/ttyUSB0"
	exit
fi

if test -c ${PORT}; then
	echo "${PORT} exists"
else
	echo "${PORT} doesn't exist"
	exit
fi

# function to echo a string, then send to port with <CR> appended

send() {
	echo $1 $''
	echo $1 $'' > ${PORT}
}

while true
do
	send "a 70"		# set start address
	send "d 0x 57ffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x ffffffff"
	send "d 0x fffff5"
	send "a fc"		# set end address

	send "c 0"
	sleep 0.5

done
