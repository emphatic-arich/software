#include <cctype>
#include <iostream>
#include <fstream>
#include <cstring>

using namespace std;

bool HexTest(char* str, uint64_t & converted_value);
bool BinTest(char* str, uint64_t & converted_value);
bool DecTest(char* str, uint64_t & converted_value);
bool NumberConvert(char* str, uint64_t & converted_value);