#include "Register_Lookup_Table.hh"

int main(){
  string name;
  vector<RegInfo> x;
  int i;
  
  x = Table("Default_Register_Values.csv");

  cout << "Enter name to translate or 'list'" << endl;

  while(1) {
    cout << "Name: ";
    getline( cin, name);
    if( name == "list" || name == "LIST") {
      for(int i=0; i < x.size(); i++)
	std::cout << x[i].Name << ' '<< x[i].Bit<<' '<< x[i].Width << ' '<<x[i].Value<<std::endl;
    } else {
      i = find_name( name, x);
      if( i < 0)
	cout << "Not found!" << endl;
      else
	cout << "First Bit:"<<x[i].Bit<<" Reg Width:"<< x[i].Width <<" Value:"<<x[i].Value<< endl;
    }
  }

} 