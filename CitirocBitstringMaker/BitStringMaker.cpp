#include "Register_Lookup_Table.hh"
#include "Number_Input_Check.hh"
#include <bitset>
#include <tgmath.h>
#include <unistd.h> // for sleep()
#include <tclap/CmdLine.h>
#include "../serial_demo_cpp/serial_command.hh"
#include "../serial_demo_cpp/serial_read_write.hh"



using namespace std;

int main(int argc, char** argv){

    /// First section of the code compiles the bitstring

    //Try block used to catch errors in command line entry
    try{

    TCLAP::CmdLine cmd("Call configuration file and run in test mode or with an active serial port", ' ', "0.9");

    // Configure input file and serial port to command line arguments
    TCLAP::ValueArg<std::string> inputfileArg("f","file","Configuration file to load",true,"homer","string");
    TCLAP::ValueArg<std::string> spArg("s","serialport","Serial Port being used",false,"homer","string");
    TCLAP::ValueArg<std::string> readinArg("r","inbitstring","Bitstring data file to load",false,"homer","string");
    TCLAP::ValueArg<std::string> writeoutArg("w","outbitstring","Bitstring data file to write",false,"homer","string");
    
    // Configure a switch to turn on test mode without a serial port connection
    TCLAP::SwitchArg testSwitch("t","testmode","Test without serial port connection", cmd, false);

    cmd.add( inputfileArg );
    cmd.add( spArg );
    cmd.add( readinArg );
    cmd.add( writeoutArg );
    
    //Parse command line and extract inputs
    cmd.parse( argc, argv );
    std::string input_file = inputfileArg.getValue();
    std::string serial_port = spArg.getValue();
    std::string data_in = readinArg.getValue();
    std::string data_out = writeoutArg.getValue();
	bool test_mode = testSwitch.getValue();


    string name;
    vector<RegInfo> Reg_Info;
    std::string total_bitstring;

    int error_flag=0;

    //Load default register values
    Reg_Info = Table(input_file);

    //Loop through all values in register
    for (int i=0; i< Reg_Info.size(); i++){
        // convert string to char* for input to NumberConvert
        char* RegValue_char= &Reg_Info[i].Value[0];

        //initialize number that will become integer for this register
        uint64_t num;

        if(NumberConvert(RegValue_char,num)){
            std::string bitstring = std::bitset<32>(num).to_string();

            //Return error if value is too large for register
            for(int j=0; j<bitstring.length()-Reg_Info[i].Width; j++){
                //if value has a 1 in a location that is out of register reach, return following error
                if(bitstring[j]=='1'){
                    cout<<"Error. Invalid input: "<< bitstring<<" for register with width "<<Reg_Info[i].Width<<". Line "<<i+1<<endl;
                    error_flag+=1;
                }
            }

            //Removing the first 32-(RegistedWidth) characters to have bitstring be the correct width
            bitstring.erase (0,(32-Reg_Info[i].Width));

	        //Correcting order of Bitstring
            std::reverse(bitstring.begin(), bitstring.end()); 

            //Reversing Bitstring for HG_GAIN and LG_GAIN 
            if (Reg_Info[i].Name.find("G_GAIN") != std::string::npos) {
                std::reverse(bitstring.begin(), bitstring.end()); 
            }

            //leftmost entry is 0th bit
            total_bitstring.append(bitstring);
        }
        else{
            cout<<"Error listed above for value of register: "<<Reg_Info[i].Name<<" located on line "<<i+1<<"."<<endl;
        }

    }

    //(Old, this was necessary for writing to Citiroc which is now obsolete in this program) reverse overall bitstring such that 0th bit is on the right
    //std::reverse(total_bitstring.begin(), total_bitstring.end());

    //If data file input is selected, overwrite default bitstring with bitstring from data file
    if (data_in!="homer"){
        ifstream data_infile(data_in);
        std::stringstream temp_bitstring;
        temp_bitstring << data_infile.rdbuf();
        total_bitstring = temp_bitstring.str();
        temp_bitstring.str("");
    }

    //check if bitstring is correct size
    if (total_bitstring.length()!=1144) {
        cout<< "Bitstring is not 1144 bits long."<<endl;
        error_flag+=1;
    }

    if(error_flag>0){ 
        cout<<"Unable to produce 1144 bitstring due to above errors"<<endl;
    }
    else{
        //cout<<total_bitstring<<endl;
        //cout<<"Bitstring length is: "<<total_bitstring.length()<<endl;
        }



    /// Serial Test using Bitstring compiled above 
    string command;

    
    //interactive mode to change individual register values
    string value;
    uint64_t number;
    int clock_div=0; //time clock spends on high signal 
    int citi_add=1; //Citiroc chip address (0 for chip A, 1 for chip B)

    if(!test_mode){
        SerialPort sp(serial_port.c_str());                  // connect to the port
        sp.SetBaudRate(LibSerial::BaudRate::BAUD_9600); // set baud rate
//	sp.SetBaudRate(LibSerial::BaudRate::BAUD_115200); // set baud rate

        while (1){

            //Bitstring testing using bitbanging (pin28=0), sending signals directly to sdata, sclock 
            cout<< "Bitstring Serial Interface. Enter 'Start' to Begin, 'Bitstring' to print bitstring, 'Reset' to reset bits, 'Edit' to change bits, or 'Write' to write out bitstring: ";
            getline(cin,command);
            if (command=="Bitstring" || command=="bitstring" || command=="b") cout<<total_bitstring<<endl;
	    
            //Commands to use when connected to serial port (not in test mode)
            if (command=="Reset" || command=="reset" || command=="r"){
	            //Reset all bits to 0 (used for testing)
	            cout<<"Resetting all bits to 0"<<endl;
		        serial_register_write(sp,0,0);
	            serial_register_write(sp,1,0);
	        }
            if (command=="Start" || command=="start" || command=="s"){
                send_string_cmd( sp, "x 1");
                send_string_cmd( sp, "a 70");

                //Initialize string array to hold 32 bit hex values
                std::string hex_values[36]={};
                int loop_tracker =0;
                std::string sub_string;
                std::stringstream hex_string;
                std::string rev_hex_string;
                std::string bit_value;
                int j = -1;
                std::hex;

                //Loop backwards through array to create Hex values to send
                for (int i = total_bitstring.length(); i --> 0; ){
                    loop_tracker+=1;
                    bit_value = total_bitstring[i];
                    sub_string.append(bit_value);
                    //Need to set hex string every 16 bits to prevent errors with integers being too large
                    if (loop_tracker==16){
                    std::reverse(sub_string.begin(), sub_string.end());
                    hex_string<<std::hex<<std::stoi(sub_string,0,2);
                        sub_string="";
                    }
                    //Store string and reset every 32 bits
                    if (loop_tracker==32 or i==0){
                        j+=1;
                        //store previous stringstream in order to push new one to the front
                        const std::string &temp = hex_string.str();
                        hex_string.seekp(0);
                        std::reverse(sub_string.begin(), sub_string.end());
                        hex_string<< std::stoi(sub_string,0,2);
                        hex_string<<temp;
                        //rev_hex_string = hex_string.str();
                        //std::reverse(rev_hex_string.begin(), rev_hex_string.end());
                        //hex_values[j] = rev_hex_string;
                        hex_values[j] = hex_string.str();
                        loop_tracker=0;
                        sub_string="";
                        hex_string.str("");
                        rev_hex_string ="";
                    }
                }
                for (int i = 0; i < 36; i++){
                    std::stringstream cmd;
                    cmd<<"d "<<hex_values[i];
                    send_string_cmd( sp, cmd.str());
                    cmd.str("");
                }

                send_string_cmd( sp, "c 0");
	        }
            if (command=="Write" || command=="write" || command=="w"){
                ofstream data_outfile(data_out);
                if(data_out!="homer"){
                    cout<<"Writing to: "<<data_out<<endl;
                    data_outfile<<total_bitstring;
                }
            }
            if (command=="Edit" || command=="edit" || command=="e") {
                cout<< "Interactive Mode (change register values) ";
                cout<< "Enter Register Name or 'List' to show all registers and default values:";

                // Variable to keep track if command entered was a correct register name
                int register_name_flag=0;

                getline( cin, name);
                for (int i=0; i< Reg_Info.size(); i++){
                    if(name==Reg_Info[i].Name){
                        register_name_flag+=1;
                        cout<<"Set register value to:"<<endl;
                        getline(cin,value);

                        //Return error if input does not match register width (not currently functional)
                        //if(value.length()!=Reg_Info[i].Width){
                        //    cout<<"Error. Invalid input: "<< value<<" for register with width "<<Reg_Info[i].Width<<endl;
                        //    break;
                        //}

                        char* value_char = &value[0];
                        if(NumberConvert(value_char,number)){
                            std::string bitstring = std::bitset<32>(number).to_string();
                            //Removing the first 32-(RegistedWidth) characters to have bitstring be the correct width
                            bitstring.erase (0,(32-Reg_Info[i].Width));

                            //Correcting order of Bitstring
                            std::reverse(bitstring.begin(), bitstring.end()); 

                            //Reversing Bitstring for HG_GAIN and LG_GAIN 
                            if (Reg_Info[i].Name.find("G_GAIN") != std::string::npos) {
                                std::reverse(bitstring.begin(), bitstring.end()); 
                            }
                            cout<<"Changing "<< Reg_Info[i].Name<< " to "<<bitstring<<endl;

                            //Reverse total bitstring to insert new string
                            //std::reverse(total_bitstring.begin(), total_bitstring.end());
                            //Chang corresponding bits
                            for (int j=0; j< Reg_Info[i].Width; j++){
                                total_bitstring[j+Reg_Info[i].Bit] = bitstring[j];
                            }
                            //Reverse bitsring back to normal
                            //std::reverse(total_bitstring.begin(), total_bitstring.end());

                                
                        }
                        else{ cout<<"Cannot change register due to above error(s)"<<endl;
                        }
                    }
                }
                if( name == "list" || name == "LIST" || name =="l") {
                    for(int i=0; i < Reg_Info.size(); i++)
                    std::cout << Reg_Info[i].Name << ' '<< Reg_Info[i].Bit<<' '<< Reg_Info[i].Width << ' '<<Reg_Info[i].Value<<std::endl; 
                }
                if(register_name_flag==0) cout<<name<<" is not a valid register name"<<endl;
            }
        }
    }


    // Interactive Test Mode
    if(test_mode){
        while (1){

            //Bitstring testing using bitbanging (pin28=0), sending signals directly to sdata, sclock 
            cout<< "Bitstring Serial Interface Test Mode. Enter 'Bitstring' to print bitstring,'Edit' to change bits, or 'Write' to write out bitstring: ";
            getline(cin,command);
            if (command=="Bitstring" || command=="bitstring" || command=="b") cout<<total_bitstring<<endl;
            if (command=="Write" || command=="write" || command=="w"){
                ofstream data_outfile(data_out);
                if(data_out!="homer"){
                    cout<<"Writing to: "<<data_out<<endl;
                    data_outfile<<total_bitstring;
                }
            }
            if (command=="Edit" || command=="edit" || command=="e") {
                cout<< "Interactive Mode (change register values) ";
                cout<< "Enter Register Name or 'List' to show all registers and default values:";

                // Variable to keep track if command entered was a correct register name
                int register_name_flag=0;

                getline( cin, name);
                for (int i=0; i< Reg_Info.size(); i++){
                    if(name==Reg_Info[i].Name){
                        register_name_flag+=1;
                        cout<<"Set register value to:"<<endl;
                        getline(cin,value);

                        //Return error if input does not match register width (not currently functional)
                        //if(value.length()!=Reg_Info[i].Width){
                        //    cout<<"Error. Invalid input: "<< value<<" for register with width "<<Reg_Info[i].Width<<endl;
                        //    break;
                        //}

                        char* value_char = &value[0];
                        if(NumberConvert(value_char,number)){
                            std::string bitstring = std::bitset<32>(number).to_string();
                            //Removing the first 32-(RegistedWidth) characters to have bitstring be the correct width
                            bitstring.erase (0,(32-Reg_Info[i].Width));

                            //Correcting order of Bitstring
                            std::reverse(bitstring.begin(), bitstring.end()); 

                            //Reversing Bitstring for HG_GAIN and LG_GAIN 
                            if (Reg_Info[i].Name.find("G_GAIN") != std::string::npos) {
                                std::reverse(bitstring.begin(), bitstring.end()); 
                            }
                            cout<<"Changing "<< Reg_Info[i].Name<< " to "<<bitstring<<endl;

                            //Reverse total bitstring to insert new string
                            //std::reverse(total_bitstring.begin(), total_bitstring.end());
                            //Chang corresponding bits
                            for (int j=0; j< Reg_Info[i].Width; j++){
                                total_bitstring[j+Reg_Info[i].Bit] = bitstring[j];
                            }
                            //Reverse bitsring back to normal
                            //std::reverse(total_bitstring.begin(), total_bitstring.end());

                                
                        }
                        else{ cout<<"Cannot change register due to above error(s)"<<endl;
                        }
                    }
                }
                if( name == "list" || name == "LIST" || name =="l") {
                    for(int i=0; i < Reg_Info.size(); i++)
                    std::cout << Reg_Info[i].Name << ' '<< Reg_Info[i].Bit<<' '<< Reg_Info[i].Width << ' '<<Reg_Info[i].Value<<std::endl; 
                }
                if(register_name_flag==0) cout<<name<<" is not a valid register name"<<endl;
            }
        }
    }
    }

    catch (TCLAP::ArgException &e) //catch exceptions
    {std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;}
    return 0;
    }
