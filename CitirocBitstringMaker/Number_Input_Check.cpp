#include "Number_Input_Check.hh"

using namespace std;

//Function testing for valid Hexadecimal number
bool HexTest(char* str, uint64_t & converted_value){
    int flag=0;
    // Loop over each character in the string
    for (int i=0; i<strlen(str); i++){
        // isxdigit checks if character is valid in hex, !isxdigit returns 1 if character is invalid
        if (!isxdigit(str[i])){
            flag+=1;
            cout<<str<<"  is not a valid hex number. ";
            cout<<str[i]<<"  is not a valid hex character."<<endl;
            break;
        }
    }
    if (flag==0){
        char *ptr;
        converted_value = strtol(str, &ptr, 16); //convert to decimal using base 16
        return 1;
    }
    else {
        return 0;
    }
}

//Function testing for valid Binary number
bool BinTest(char* str, uint64_t & converted_value){
    int flag=0;
    // Loop over each character in the string
    for (int i=0; i<strlen(str); i++){
        // if character in string is not a 0 or 1 then return error
        if (str[i]!='0' && str[i]!='1'){
            flag+=1;
            cout<<str<<"  is not a valid binary number. ";
            cout<<str[i]<<"  is not a valid binary character."<<endl;
            break;
        }
    }
    if (flag==0){
        char *ptr;
        converted_value = strtol(str, &ptr, 2); //convert to decimal using base 2
        return 1;
    }
    else {
        return 0;
    }
}

//Function testing for valid Decimal
bool DecTest(char* str, uint64_t & converted_value){
    int flag=0; 
    // Loop over each character in the string
    for (int i=0; i<strlen(str); i++){
        // isdigit checks if character is valid in decimal, !isxdigit returns 1 if character is invalid
        if (!isdigit(str[i])){
            flag+=1;
            cout<<str<<"  is not a valid decimal number. ";
            cout<<str[i]<<"  is not a valid decimal character."<<endl;
            break;
        }
    }
    if (flag==0){
        char *ptr;
        converted_value = strtol(str, &ptr, 10); //convert to decimal using base 10
        return 1;
    }
    else {
        return 0;
    }
}

//Main function to check any number
bool NumberConvert(char* str, uint64_t & converted_value){
    // Number should begin with 0x, 0b, or 0d
    // If first character is not a 0, assume decimal
    if (str[0]!='0'){
        if(DecTest(str,converted_value)) return 1;
        else return 0; 
    }

     //If input is just "0" ensure it is a valid integer
    else if (strlen(str)==1){
        converted_value=0;
        return 1;
    }

    // If first two characters are 0x or 0X, check Hex
    else if (str[1]=='x' || str[1]=='X'){
        //remove 0x by creating a substring removing first two entries
        char* substr = str + 2;
        if(HexTest(substr, converted_value)) return 1;
        else return 0; 
    }
    // If first two characters are 0b or 0B, check Binary
    else if (str[1]=='b' || str[1]=='B'){
        //remove 0x by creating a substring removing first two entries
        char* substr = str + 2;
        if(BinTest(substr, converted_value)) return 1;
        else return 0; 
    }
    // If first two characters are 0b or 0B, check Binary
    else if (str[1]=='d' || str[1]=='D'){
        //remove 0x by creating a substring removing first two entries
        char* substr = str + 2;
        if(DecTest(substr,converted_value)) return 1;
        else return 0; 
    } 
    else{
        cout<<str<<" is not a valid input. ";
        cout<<str[0]<<str[1]<<" is not a valid indicator. Please use 0x for hexadecimal, 0b for binary, or 0d for decimal."<<endl;
        return 0;
    }
}

