#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>

using namespace std;

struct RegInfo {
  std::string Name;
  int Bit;
  int Width;
  std::string Value;
};

vector<RegInfo> Table( string fname);
int find_name( string name, vector<RegInfo> v);