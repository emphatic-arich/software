#include "Number_Input_Check.hh"

using namespace std;

int main(){
    //reading test input file, validating each line as a number
    ifstream file("input.txt");
    string line;
    uint64_t num;
    while(file >> line) {
        //convert line to character array
        char* numstr;
        numstr= &line[0];
        
        if(NumberConvert(numstr,num)){
            cout<<"Base 10 value of "<<numstr<<" is "<<num<<endl;
        }
    }
    return 0;
}